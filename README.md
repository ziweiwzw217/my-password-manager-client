# Getting Started 

This project requires a [backend](https://github.com/WilsonZiweiWang/my_password_manager_backend). Clone the backend and get it running before initializing the client.
# Run
## Local

Run `npm install` then run `npm start`.

It will run the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.
## Docker

To run the project in Docker, you need to install the dependencies first on the host machine at the root directory:

```
npm install
```

Next:

```
docker-compose up
```

More about docker [here](https://docs.docker.com/)

## Environment

Create a `.env` file and include the following variables:

 - **REACT_APP_AES_KEY** \
    This is key is for the AES encryption and decryption. 


