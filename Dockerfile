FROM node:16-alpine

WORKDIR /code

COPY package-lock.json /code/
COPY package.json /code/

RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm", "run", "start"]
