import React from 'react';
import './css/App.css';
import SignIn from './components/signIn';
import Register from './components/register';
import Otp from './components/otp';
import Home from './pages/home';
import ResetPassword from './pages/resetPassword';
import Admin from './pages/admin/admin';
import PageNotFound from './pages/page404';

import GlobalModal from './components/globalModal';

import { BrowserRouter, Routes, Route } from 'react-router-dom';
import AuthProvider from './context/authContext';
function App() {
  return (
    <>
      <BrowserRouter>
        <div className='App'>
          <AuthProvider>
            <GlobalModal />
            <Routes>
              <Route path='/' exact element={<Home />} />
              <Route path='/login' element={<SignIn />} />
              <Route path='/register' element={<Register />} />
              <Route path='/otp' element={<Otp />} />
              <Route path='/home' element={<Home />} />
              <Route path='/reset-password' element={<ResetPassword />} />
              <Route path='/admin/login' element={<SignIn />} />
              <Route path='/admin/residents' exact element={<Admin />} />
              <Route path='/404' element={<PageNotFound />} />
              <Route path='*' element={<PageNotFound />} />
            </Routes>
          </AuthProvider>
        </div>

      </BrowserRouter>

    </>
  );
}

export default App;
