import { AES } from "crypto-js";
import CryptoJS from "crypto-js";

export const encryptPassword = (password) => {
    return AES.encrypt(password, process.env.REACT_APP_AES_KEY).toString();
}

export const decryptPassword = (password) => {
    let bytes = AES.decrypt(password, process.env.REACT_APP_AES_KEY);
    return bytes.toString(CryptoJS.enc.Utf8);
}