import file from './output-records.json';
import * as URL from '../constants/urls';
import axios from 'axios';
import { encryptPassword } from './aes';
export const importRecordFromJSON = () => {
    console.log({ file });
    for (let item of file) {
        axios({
            url: URL.CREATE_RECORD,
            method: 'POST',
            data: {
                account: item.account,
                siteName: item.siteName,
                password: encryptPassword(item.password),
                customtextfields: item.customtextfields
            }
        }).then(res => {
            if (res.status === 201) {
                // call modal
                console.log('created');
            }
            else {
                // display messages
                console.log(res);
            }
        }).catch(err => {
            console.log(err);
        })
    }

}

