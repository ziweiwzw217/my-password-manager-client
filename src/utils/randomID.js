export const getRandomId = (len) => {
    let randomId = '';
    let character = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    while(randomId.length < len){
        let index = Math.floor(Math.random() * character.length);
        if(index === character.length){
            continue;
        }
        randomId += character[index]
    }

    return randomId;
}