// import React from 'react';
import axios from 'axios';
import jwt_decode from 'jwt-decode';
import { useContext } from 'react';
import { AuthContext } from '../context/authContext';
import * as URL from '../constants/urls';
import dayjs from 'dayjs';


const useAxios = () => {
    const { authToken, setUser, setAuthToken } = useContext(AuthContext);

    const axiosInstance = axios.create({
        headers: {
            Authorization: `Bearer ${authToken?.access}`
        }
    });

    axiosInstance.interceptors.request.use(async req => {
        const user = jwt_decode(authToken.access);
        const isExpired = dayjs.unix(user.exp).diff(dayjs()) < 1;//can use js built-in date() to achieve this?
        
        if(!isExpired) {
            console.log('not expired', req)
            return req;
        }
        const response = await axios.post(URL.REFRESH_TOKEN, {
            refresh: authToken.refresh
        })

        localStorage.setItem('authToken', JSON.stringify(response.data));

        setAuthToken(response.data);
        setUser(jwt_decode(response.data.access));

        req.headers.Authorization = `Bearer ${response.data.access}`;
        console.log('expired', req)
        return req;
    })

    return axiosInstance;

}

export default useAxios;
