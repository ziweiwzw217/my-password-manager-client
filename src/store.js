import { configureStore } from '@reduxjs/toolkit';
import recordFormReducer from './reduxStore/records/recordSlice';
import modalReducer from './reduxStore/modal/modalSlice';
import apiReducer from './reduxStore/api/apiSlice';

export const store = configureStore({
    reducer: {
        recordForm: recordFormReducer,
        modal: modalReducer,
        api: apiReducer,
    },
})