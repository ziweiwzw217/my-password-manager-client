import React, { createContext, useState, useEffect } from "react";
import jwt_decode from 'jwt-decode';
import { useNavigate } from "react-router-dom";
import * as URL from '../constants/urls';

export const AuthContext = createContext();

const AuthProvider = ({ children }) => {
    const navigate = useNavigate();
    const [authToken, setAuthToken] = useState(() => localStorage.getItem('authToken') ? JSON.parse(localStorage.getItem('authToken')) : null);
    
    const [user, setUser] = useState(() => localStorage.getItem('authToken') ? jwt_decode(localStorage.getItem('authToken')) : null);

    const [isAdminLogin, setIsAdminLogin] = useState(window.location.href.includes('admin/login'));

    const [isAdmin, setIsAdmin] = useState(() => localStorage.getItem('isAdmin') ? JSON.parse(localStorage.getItem('isAdmin')) : false);
    
    const [user_verified, setUser_verified] = useState(false);

    const [modalMessage, setModalMessage] = useState('');
    const [modalType, setModalType] = useState('');
    const [modalShow, setModalShow] = useState(false);


    const loginUser = async (username, password) => {
        let msg = [];
        const res = await fetch(URL.USER_LOGIN_CUSTOM, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username, password
            })
        })
        console.log({ res });

        const data = await res.json();
        console.log({ data });
        
        if (res.status === 200) {
            setAuthToken(data?.token);
            setUser(jwt_decode(data?.token.access));
            localStorage.setItem('authToken', JSON.stringify(data?.token));
            setIsAdmin(false); //make sure admin access is not available
            localStorage.setItem('isAdmin', JSON.stringify(data?.isAdmin))
                
            navigate('/otp');
        }
        else if (res.status === 401) {
            msg.push(data.message);
            enableModal(msg, 'error');
        }
        else{
            console.error(res.status, res.statusText);
            msg.push(data.message);
            enableModal(msg, 'error');
          
        }

    }

    const loginAdminUser = async (username, password) => {
        let msg = [];
        const res = await fetch(URL.USER_LOGIN_CUSTOM, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username, password
            })
        })
        console.log({ res });

        const data = await res.json();
        console.log({ data });

        
        if (res.status === 200) {
            if(!data.isAdmin){
                msg.push('you are not admin, redirecting to standard login page');
                enableModal(msg, 'error');
                setIsAdminLogin(false);
                logoutUser();
                navigate('/login');
                return;
            }
            setAuthToken(data?.token);
            setUser(jwt_decode(data?.token.access));
            localStorage.setItem('authToken', JSON.stringify(data?.token));
            localStorage.setItem('isAdmin', JSON.stringify(data?.isAdmin))
            setIsAdmin(true);
            navigate('/admin/residents');
        }
        else if (res.status === 401) {
            msg.push(data.message);
            enableModal(msg, 'error');
        }
        else{
            console.error(res.status, res.statusText);
            msg.push(data.message);
            enableModal(msg, 'error');
          
        }

    }

    const registerUser = async (username, email, password, password2) => {
        let msg = [];
        //already handled by <input/>
        let isValid = (username && email && password && password2);
        if (!isValid){
            msg.push('all fields are required');
            enableModal(msg, 'error');
            return;
        }
        isValid = password === password2;
        if(!isValid){
            msg.push('passwords should be the same');
            enableModal(msg, 'error');
            return;
        }
        

        const res = await fetch(URL.USER_REGISTER, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username, email, password, password2
            })
        });
        console.log({ res });
        let data = res.json();
        console.log({data})
        if (res.status === 201) {
            navigate('/login');
        }
        else {
            data.then(result => {
                console.log(result)
                Object.keys(result).forEach(key => {
                    let item = result[key];
                    for(let i=0; i< item.length; i++){
                        msg.push(item[i]);
                    }
                })

                enableModal(msg, 'error');
            })
        }
    }

    const logoutUser = () => {
        //send request to logout user? (make token outstanding in backend)
        setAuthToken(null);
        setUser(null);
        setUser_verified(false);
        localStorage.removeItem('authToken');
        localStorage.removeItem('isAdmin');
        navigate('/login');
    }

    const showModalAndAutoHide = () => {
        setModalShow(true);

        setTimeout(() => {
            setModalShow(false);
        }, 5000);
    }
  
    const enableModal = (msg, type) => {
        setModalMessage(msg);
        setModalType(type);
        showModalAndAutoHide();
    }

    const contextData = {
        user,
        setUser,
        authToken,
        setAuthToken,
        registerUser,
        loginUser,
        logoutUser,
        user_verified,
        setUser_verified,
        loginAdminUser,

        isAdmin,

        modalMessage,
        setModalMessage,
        modalType,
        setModalType,
        modalShow,
        setModalShow,
        enableModal,

        isAdminLogin,
        setIsAdminLogin,
    }

    useEffect(() => {
        if (authToken) {
            setUser(jwt_decode(authToken.access));
        }
        // setIsLoading(false);
    }, [authToken])

    return (
        <AuthContext.Provider value={contextData}>
            {/* {isLoading ? (<div>Loading</div>) : children} */}
            {children}
        </AuthContext.Provider>
    )
}

export default AuthProvider;