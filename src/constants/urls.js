const HOST = 'http://127.0.0.1:3001';
const BASE_URL = `${HOST}/myPasswordManager`;
// const HOST = 'http://127.0.0.1:9000';
export const SEARCH_RECORD = `${BASE_URL}/getRecord`;
export const CREATE_RECORD = `${BASE_URL}/createRecord/`;
export const UPDATE_RECORD = `${BASE_URL}/updateRecord/`;
export const DELETE_RECORD = `${BASE_URL}/deleteRecord/`;


export const USER_LOGIN = `${BASE_URL}/token-default/`;
export const USER_LOGIN_CUSTOM = `${BASE_URL}/token-custom/`;
export const USER_REGISTER = `${BASE_URL}/register/`;
export const GET_OTP = `${BASE_URL}/otp/`;
export const REFRESH_TOKEN = `${BASE_URL}/token/refresh/`;
export const VERIFY_OTP = `${BASE_URL}/otp-verify/`;
export const RESET_PASSWORD_REQUEST = `${BASE_URL}/reset-password-request/`;
export const RESET_PASSWORD_REQUEST_VERIFY = `${BASE_URL}/verify-reset-password-request/`;
export const RESET_PASSWORD = `${BASE_URL}/reset-password/`;

