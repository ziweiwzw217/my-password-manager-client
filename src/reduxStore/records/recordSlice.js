import { createSlice } from '@reduxjs/toolkit';
import { getRandomId } from '../../utils/randomID';
const initialState = {
    //Empty record form, never alter with dispatch
    emptyRecordFormData: {
        siteName: '',
        account: '',
        password: '',
        id: null,
    },
    //Store the form data before any edition
    initialRecordFormData: {
        siteName: '',
        account: '',
        password: '',
        id: null,
    },
    //Store the current form data
    recordFormData: {
        siteName: '',
        account: '',
        password: '',
        id: null,
    },
    isEdited: false,
    editMode: false,
    formIsOpen: false,
    newRecordMode: false,

    showSelectCustomField: false,
    selectedCustomFieldType: '',
    customFieldOptions: [
        '',
        'email',
        'security question',
        'text',
    ],
    // The custom fields that the current record has
    customFields: [],

}

export const recordFormSlice = createSlice({
    name: 'record',
    initialState,
    reducers: {
        onEditRecordMode: (state, action) => {
            state.formIsOpen = true;
            state.newRecordMode = false;
            state.recordFormData = action.payload;
            state.customFields = action.payload.customtextfields;
            state.initialRecordFormData = state.recordFormData;
            state.showSelectCustomField = false;
        },
        updateRecordForm: (state, action) => {
            state.recordFormData = {
                ...state.recordFormData,
                [action.payload.key]: action.payload.value
            };
        },
        resetRecordForm: (state) => {
            state.recordFormData = state.initialRecordFormData;
        },

        setIsEdited: (state, action) => {
            state.isEdited = action.payload;
        },
        setEditMode: (state, action) => {
            state.editMode = action.payload;
        },
        exitEditMode: (state) => {
            state.isEdited = false;
            state.editMode = false;
        },
        onNewRecordMode: (state) => {
            state.formIsOpen = true;
            state.newRecordMode = true;
            state.editMode = false;
            state.recordFormData = state.emptyRecordFormData;
            state.initialRecordFormData = state.emptyRecordFormData;
            state.customFields = [];
            state.selectedCustomFieldType = '';
            state.showSelectCustomField = false;
        },
        setShowSelectCustomField: (state, action) => {
            state.showSelectCustomField = action.payload;
            if (action.payload === false) {
                state.selectedCustomFieldType = '';
            }
        },
        onSelectCustomFieldType: (state, action) => {
            state.selectedCustomFieldType = action.payload;
        },
        onAddCustomField: (state, action) => {
            state.customFields.push({
                id: `temporary-${action.payload.type}-${getRandomId(6)}`, //temporary id for React, it is removed on sending to backend
                field_name: '',
                field_value: '',
                field_type: action.payload.type
            });
            state.showSelectCustomField = false;
            state.selectedCustomFieldType = '';
        },
        onModifyCustomField: (state, action) => {
            let index = state.customFields.findIndex(item => item.id === action.payload.id);
            state.customFields[index].field_name = action.payload?.field_name ?? state.customFields[index].field_name;
            state.customFields[index].field_value = action.payload?.field_value ?? state.customFields[index].field_value;
        },
        onRemoveCustomField: (state, action) => {
            state.customFields = state.customFields.filter((field) => field.id !== action.payload);
        },
        toggleFormIsOpen: (state) => {
            state.formIsOpen = !state.formIsOpen;
        }
    }
})


export const {
    updateRecordForm,
    resetRecordForm,
    onEditRecordMode,
    setIsEdited,
    setEditMode,
    exitEditMode,
    onNewRecordMode,
    onSelectCustomFieldType,
    setShowSelectCustomField,
    onAddCustomField,
    onModifyCustomField,
    onRemoveCustomField,
    toggleFormIsOpen
} = recordFormSlice.actions;

export default recordFormSlice.reducer;