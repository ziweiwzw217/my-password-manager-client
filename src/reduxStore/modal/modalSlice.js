import { createSlice } from '@reduxjs/toolkit';
import { deleteRecord } from '../api/apiSlice';

const initialState = {
    showModal: false,
    modalMessage: '',
    modalAction: [],
    actionContext: null,
    actions: {
        confirmDelete: {
            name: 'confirmDelete',
            color: 'primary',
            variant: 'outlined',
            text: 'confirm',
        },
        ok: {
            name: 'ok',
            color: 'success',
            variant: 'outlined',
            text: 'ok',
        },
        cancel: {
            name: 'cancel',
            color: 'primary',
            variant: 'contained',
            text: 'cancel',
        },
        confirmExit: {
            name: 'confirmExit',
            color: 'primary',
            variant: 'outlined',
            text: 'confirm',
        }
    },
    messages: {
        onDelete: 'Do you want to delete this record? This action cannot be undone.',
        onExitFormWithouSaving: 'You have unsaved changes, do you want to exit the form now?',
    }
}

export const modalSlice = createSlice({
    name: 'modal',
    initialState,
    reducers: {
        setShowModal: (state, action) => {
            state.showModal = action.payload;
        },
        onConfirmDeleteRecord: (state, action) => {
            state.showModal = true;
            state.modalMessage = state.messages.onDelete;
            state.actionContext = action.payload; //assign the id which can be later passed to the api action
            state.modalAction = [
                state.actions.confirmDelete,
                state.actions.cancel,
            ]
        },
        onCloseModal: (state) => {
            state.showModal = false;
            state.modalMessage = '';
            state.modalAction = [];
            state.actionContext = null;
        },
        onExitFormWithouSaving: (state) => {
            state.showModal = true;
            state.modalMessage = state.messages.onExitFormWithouSaving;
            state.modalAction = [
                state.actions.confirmExit,
                state.actions.cancel,
            ]
        },
    },
    extraReducers(builder){
        builder.addCase(deleteRecord.fulfilled, (state) => {
            state.showModal = false;
            state.modalMessage = '';
            state.modalAction = [];
            state.actionContext = null;
        })
    }
})

export const {
    setShowModal,
    onConfirmDeleteRecord,
    onCloseModal,
    onExitFormWithouSaving
} = modalSlice.actions;


export default modalSlice.reducer;

