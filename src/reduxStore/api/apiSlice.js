import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import * as URL from '../../constants/urls';
import axios from 'axios';
import { exitEditMode, onEditRecordMode, toggleFormIsOpen } from '../records/recordSlice';
import { decryptPassword, encryptPassword } from '../../utils/aes';

const initialState = {
    records: [],
    searchResults: [],
    isLoading: false,
    recordId: null, // For storing the id of the record to be deleted etc
}

export const getRecordById = createAsyncThunk('record/getById', (id, { dispatch }) => {
    return axios({
        url: URL.SEARCH_RECORD,
        method: 'GET',
        params: {
            id
        }
    }).then((res) => {
        if (res.status === 200) {
            let dataWithDecryptedPassword = {
                ...res.data,
                password: decryptPassword(res.data.password)
            }
            dispatch(onEditRecordMode(dataWithDecryptedPassword));
        }
        else if (res.status === 404) {
            console.log('Record not found');
        }
    }).catch((err) => {
        console.log(err);
    })
})

export const searchRecord = createAsyncThunk('record/search', (searchQuery) => {
    return axios({
        url: URL.SEARCH_RECORD,
        method: 'GET',
        params: {
            siteName: searchQuery
        }
    }).then(res => {
        console.log(res);
        if (res.status === 200) {
            return res.data;
        }
        else if (res.status === 204) {
            return [];
        }
    }).catch(err => {
        console.log(err);
    })
})

export const getAllRecord = createAsyncThunk('record/getAll', (isCancelled) => {
    return axios({
        url: URL.SEARCH_RECORD,
        method: 'GET',
        params: {
            siteName: ''
        }
    }).then(res => {
        if (!isCancelled) {
            console.log(res);
            if (res.status === 200) {
                // data returned here can be passed to the 'action' when fulfilled
                return res.data;
            }
            else if (res.status === 204) {
                return [];
            }
        }
    }).catch(err => {
        console.log(err);
    })
})

export const deleteRecord = createAsyncThunk('record/delete', (id, { dispatch }) => {
    return axios({
        url: URL.DELETE_RECORD,
        method: 'DELETE',
        data: {
            "id": [id]
        }
    }).then(res => {
        console.log(res);
        if (res.status === 204) {
            // show notification 
            console.log('deleted');

            dispatch(getAllRecord());
        }
        else if (res.status === 200) {
            // some records were not found
            console.log(res);
        }
    }).catch(err => {
        console.log(err);
    })
})

export const updateRecord = createAsyncThunk('record/update', ({recordFormData, filteredCustomFields}, { dispatch }) => {
    return axios({
        url: URL.UPDATE_RECORD,
        method: 'PUT',
        data: {
            account: recordFormData.account,
            siteName: recordFormData.siteName,
            password: encryptPassword(recordFormData.password),
            id: recordFormData.id,
            customtextfields: filteredCustomFields
        }
    }).then(res => {
        if (res.status === 202) {
            // call modal
            console.log('updated');
            dispatch(exitEditMode());
        }
        else {
            // display messages
            console.log(res);
        }
    }).catch(err => {
        console.log(err);
    })
})

export const addNewRecord = createAsyncThunk('record/add', ({recordFormData, filteredCustomFields}, { dispatch }) => {
    return axios({
        url: URL.CREATE_RECORD,
        method: 'POST',
        data: {
            account: recordFormData.account,
            siteName: recordFormData.siteName,
            password: encryptPassword(recordFormData.password),
            customtextfields: filteredCustomFields
        }
    }).then(res => {
        if (res.status === 201) {
            // call modal
            console.log('created');
            dispatch(exitEditMode());
            dispatch(getAllRecord());
            dispatch(toggleFormIsOpen());
        }
        else {
            // display messages
            console.log(res);
        }
    }).catch(err => {
        console.log(err);
    })
})

// extraReducers are for actions defined outside the createSlice()
export const apiSlice = createSlice({
    name: 'api',
    initialState,
    reducers: {
        setRecordId: (state, action) => {
            state.recordId = action.payload;
        }
    },
    extraReducers(builder) {
        builder
            // get a record by id
            .addCase(getRecordById.pending, (state) => {
                state.isLoading = true;
            })
            .addCase(getRecordById.fulfilled, (state, action) => {
                console.log({ action });
                state.isLoading = false;
            })
            .addCase(getRecordById.rejected, (state) => {
                state.isLoading = false;
            })
            // get all records
            .addCase(getAllRecord.pending, (state) => {
                state.isLoading = true;
            })
            .addCase(getAllRecord.fulfilled, (state, action) => {
                state.isLoading = false;
                state.searchResults = action.payload;
            })
            .addCase(getAllRecord.rejected, (state) => {
                state.isLoading = false;
            })
            // search records
            .addCase(searchRecord.pending, (state) => {
                state.isLoading = true;
            })
            .addCase(searchRecord.fulfilled, (state, action) => {
                console.log({ action });
                state.isLoading = false;
                state.searchResults = action.payload;
            })
            .addCase(searchRecord.rejected, (state) => {
                state.isLoading = false;
            })
            // delete records
            .addCase(deleteRecord.pending, (state) => {
                state.isLoading = true;
            })
            .addCase(deleteRecord.fulfilled, (state, action) => {
                console.log({ action });
                state.isLoading = false;
            })
            .addCase(deleteRecord.rejected, (state) => {
                state.isLoading = false;
            })
            // update record
            .addCase(updateRecord.pending, (state) => {
                state.isLoading = true;
            })
            .addCase(updateRecord.fulfilled, (state, action) => {
                console.log({ action });
                state.isLoading = false;
            })
            .addCase(updateRecord.rejected, (state) => {
                state.isLoading = false;
            })
            // add record
            .addCase(addNewRecord.pending, (state) => {
                state.isLoading = true;
            })
            .addCase(addNewRecord.fulfilled, (state, action) => {
                console.log({ action });
                state.isLoading = false;
            })
            .addCase(addNewRecord.rejected, (state) => {
                state.isLoading = false;
            })


    }

})

export const { setRecordId } = apiSlice.actions;

export default apiSlice.reducer;



