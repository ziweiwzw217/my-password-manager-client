import React, { useEffect, useState } from 'react';
import { useContext } from 'react';
import { AuthContext } from '../context/authContext';
import { Navigate, useSearchParams, useNavigate } from 'react-router-dom';
import useAxios from '../utils/useAxios';
import * as URL from '../constants/urls';
import '../css/resetPassword.css';
import { FormControl, InputLabel, OutlinedInput, Button } from '@mui/material';

const ResetPassword = () => {
    const navigate = useNavigate();
    const [searchParams] = useSearchParams();
    const axios_private = useAxios();
    const { user, logoutUser, enableModal } = useContext(AuthContext);

    const [token] = useState(searchParams.get('token') || null);
    const [password, setPassword] = useState('');
    const [password2, setPassword2] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
        let msg = [];
        if (password !== password2) {
            msg.push('passwords should be the same');
            enableModal(msg, 'error');
            return;
        }
        if (password.length < 8) {
            msg.push('this password is too short, it should contain at least 8 characters');
            enableModal(msg, 'error');
            return;
        }
        axios_private({
            url: URL.RESET_PASSWORD,
            method: 'POST',
            data: {
                token,
                email: user.email,
                password,
            }
        })
            .then(res => {
                console.log(res)
                logoutUser();
            })
            .catch(err => {
                console.log(err)
                logoutUser();
                //or 404 page
            })
    }

    useEffect(() => {
        axios_private({
            url: URL.RESET_PASSWORD_REQUEST_VERIFY,
            method: 'POST',
            data: {
                token,
                email: user.email
            }
        })
            .then(res => {
                console.log(res)
            })
            .catch(err => {
                console.log(err)
                navigate('/login'); //or 404 page
            })

    }, [])

    return (
        <>
            {!user ? <Navigate to={'/login'} /> :
                (
                    <div className='reset-password'>
                        <div className='reset-password__wrapper'>

                            <h2>Reset Password</h2>
                            <form onSubmit={handleSubmit}>
                                <FormControl fullWidth sx={{ mb: 1 }}>
                                    <InputLabel htmlFor="outlined-adornment-password">{'password'}</InputLabel>
                                    <OutlinedInput
                                        sx={{ backgroundColor: '#fff' }}
                                        id="outlined-adornment-password"
                                        value={password}
                                        onChange={(e) => setPassword(e.target.value)}
                                        label={'password'}
                                        type='password'
                                    />
                                </FormControl><FormControl fullWidth sx={{ mb: 1 }}>
                                    <InputLabel htmlFor="outlined-adornment-password2">{'password2'}</InputLabel>
                                    <OutlinedInput
                                        sx={{ backgroundColor: '#fff' }}
                                        id="outlined-adornment-password2"
                                        value={password2}
                                        onChange={(e) => setPassword2(e.target.value)}
                                        label={'password2'}
                                        type='password'
                                    />
                                </FormControl>
                                <Button type='submit' variant='contained' sx={{ mt: 1, mb: 1, color: '#000', backgroundColor: '#61DAFB' }}>reset</Button>

                            </form>
                        </div>

                    </div>
                )}
        </>
    )
}

export default ResetPassword;