import React from 'react';
import {
    Button,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    Box,
    Stack,
} from '@mui/material';
import Navbar from '../../components/navbar';
import { useState } from 'react';

import { useNavigate, Navigate } from 'react-router-dom';
import { useContext } from 'react';
import { AuthContext } from '../../context/authContext';

const Admin = () => {
    const { isAdmin } = useContext(AuthContext);
    const navigate = useNavigate();
    const [residentList] = useState([]);

    return (
        <>
            {!isAdmin && <Navigate to={'/404'} />}
            <Stack direction={'column'} sx={{
                position: 'relative',
                height: 1,
            }}>
                <Navbar />
                <Box sx={{
                    background: '#fff',
                    width: 1,
                    position: 'relative',
                    pt: 1,
                }} style={{ flexGrow: 1 }}>

                    <Box sx={{
                        width: 1,
                        maxWidth: 900,
                        m: 'auto',
                    }}>
                        <Button variant={'contained'} sx={{ mt: 1, mb: 2, color: '#000', backgroundColor: '#61DAFB', }} onClick={() => navigate('/admin/residents/add', { state: { title: 'add resident' } })}>add</Button>
                        <DenseTable residentList={residentList || []} />
                    </Box>
                </Box>
            </Stack>
        </>
    )
}

const DenseTable = ({ residentList }) => {
    const navigate = useNavigate();

    return (
        <>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
                    <TableHead>
                        <TableRow>
                            <TableCell>username</TableCell>
                            <TableCell align="right">Email</TableCell>
                            <TableCell align="right">First&nbsp; name</TableCell>
                            <TableCell align="right">Last&nbsp; name</TableCell>
                            <TableCell align="right">Phone</TableCell>
                            <TableCell align="center">Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {residentList.map((resident) => (
                            <TableRow
                                key={resident.email}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {resident.username}
                                </TableCell>
                                <TableCell align="right">{resident.email}</TableCell>
                                <TableCell align="right">{resident.firstname}</TableCell>
                                <TableCell align="right">{resident.lastname}</TableCell>
                                <TableCell align="right">{resident.phone}</TableCell>
                                <TableCell align="center">
                                    <Button onClick={() => navigate(`/admin/residents/${resident.username}`, { state: { title: 'detail' } })}>detail</Button>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </>
    )
}

export default Admin;