import React from 'react';

const PageNotFound = () => {
  return (
    <h1 style={{color: '#fff'}}>Page Not Found - 404</h1>
  )
}

export default PageNotFound;