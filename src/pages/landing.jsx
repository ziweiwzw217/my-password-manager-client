import React from 'react';
import '../css/landing.css';
import { useNavigate } from 'react-router-dom';
import { Button } from '@mui/material';
const Landing = () => {
    const navigate = useNavigate();
    return (
        <div className='landing'>
            <div className='landing__wrapper'>
                <h1 className='landing__title'><span>Welcome to</span> My Resident</h1>
                <div className='landing__button__wrapper'>
                    <Button type='button' variant='contained' sx={{ mt: 1, mb: 1, color: '#000', backgroundColor: '#61DAFB' }} onClick={() => navigate('/login')}>log in</Button>

                    <span style={{alignSelf: 'center'}}>or</span>

                    <Button type='button' variant='contained' sx={{ mt: 1, mb: 1, color: '#000', backgroundColor: '#61DAFB' }} onClick={() => navigate('/register')}>register</Button>


                </div>

            </div>

        </div>
    )
} 

export default Landing;