import React, { useState, useEffect } from 'react';
import {
    FormControl, InputLabel, OutlinedInput, Box, Stack, IconButton
} from '@mui/material';
import Tooltip from '@mui/material/Tooltip';
import InputAdornment from '@mui/material/InputAdornment';
import SearchIcon from '@mui/icons-material/Search';
import CancelIcon from '@mui/icons-material/Cancel';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import UploadIcon from '@mui/icons-material/Upload';
import DeleteIcon from '@mui/icons-material/Delete';
import Divider from '@mui/material/Divider';
import '../css/home.css';
import RecordForm from '../components/recordForm.jsx';

import { useDispatch, useSelector } from 'react-redux';
import { onNewRecordMode } from '../reduxStore/records/recordSlice';
import { onConfirmDeleteRecord } from '../reduxStore/modal/modalSlice';
import { getAllRecord, searchRecord, getRecordById } from '../reduxStore/api/apiSlice';
import logo from '../images/mpm-logo.png';
import { importRecordFromJSON } from '../utils/importData';
const Home = () => {
    const dispatch = useDispatch();

    const searchResults = useSelector((state) => state.api.searchResults);

    const [searchQuery, setSearchQuery] = useState('');
    const search = (searchQuery) => {
        if (searchQuery === '') {
            dispatch(getAllRecord());
            return;
        }

        dispatch(searchRecord(searchQuery));
    }

    const editForm = (id) => {
        dispatch(getRecordById(id));
    }

    const editNewRecord = () => {
        dispatch(onNewRecordMode(true));
    }

    const importRecords = () => {
        importRecordFromJSON();
    }

    //Get first 20 records in the DB (In alphabetic order)
    useEffect(() => {
        let isCancelled = false;
        dispatch(getAllRecord(isCancelled));
        return () => {
            isCancelled = true;
        }
    }, [])

    const showModalDeleteRecord = (id) => {
        dispatch(onConfirmDeleteRecord(id));
    }

    return (
        <>
            <Box className='home'>
                <RecordForm />
                <Box className='home__wrapper'>
                    <Box className='home__sidebar'>
                        <Box className='home__sidebar-logo'>
                            <img src={logo} alt='mpm-logo' />
                        </Box>
                        <Box className='home__sidebar-actions'>
                            <Tooltip title="New Record" placement='right'>
                                <IconButton onClick={() => editNewRecord()}>
                                    <AddCircleOutlineIcon />
                                </IconButton>
                            </Tooltip>
                            <Tooltip title="Import Records" placement='right'>
                                <IconButton onClick={() => importRecords()}>
                                    <UploadIcon />
                                </IconButton>
                            </Tooltip>
                        </Box>
                    </Box>
                    <Box className='home__content' >
                        <Box className='home__header'>
                            <Box className='home__search-form-control' >
                                <FormControl className='home__search-form__input' size='small' variant='outlined'>
                                    <InputLabel htmlFor='outlined-adornment-searchQuery'>{'search query'}</InputLabel>
                                    <OutlinedInput
                                        className='home__search-form-control__input'
                                        id='outlined-adornment-searchQuery'
                                        value={searchQuery}
                                        onChange={(e) => setSearchQuery(e.target.value)}
                                        label={'search query'}
                                        required
                                        endAdornment={
                                            <InputAdornment position='end'>
                                                <IconButton
                                                    aria-label='remove input'
                                                    onClick={() => setSearchQuery('')}
                                                    onMouseDown={(e) => e.preventDefault()}
                                                    edge='end'
                                                >
                                                    <CancelIcon sx={{ visibility: searchQuery ? 'visible' : 'hidden' }} />
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                    />
                                </FormControl>
                                <IconButton onClick={() => search(searchQuery)}>
                                    <SearchIcon />
                                </IconButton>
                            </Box>
                        </Box>

                        <Stack className='search-results' divider={<Divider orientation='horizontal' flexItem />} spacing={1}>
                            {
                                searchResults.length &&
                                searchResults.map((item) => (
                                    <Box className='search-results__item' key={item.id}>
                                        <Box className='search-results__item__info' onClick={() => editForm(item.id)}>
                                            <Box>{item.siteName}</Box>
                                            <Box>{item.account}</Box>
                                        </Box>
                                        <IconButton className='search-results__item__delete' onClick={() => showModalDeleteRecord(item.id)}>
                                            <DeleteIcon color='error' />
                                        </IconButton>
                                    </Box>
                                ))
                            }
                        </Stack>
                    </Box>

                </Box>

            </Box>
        </>
    )
}

export default Home;