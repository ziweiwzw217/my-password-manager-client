import React, { useEffect, useState } from 'react';


import {
  Button, Box, Stack, Typography, TextField, FormControl,
  IconButton, Tooltip, InputLabel, Select, OutlinedInput, MenuItem
} from '@mui/material';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import DeleteIcon from '@mui/icons-material/Delete';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import '../css/recordForm.css';

import {useSelector, useDispatch} from 'react-redux';
import { updateRecordForm, setIsEdited, exitEditMode, onSelectCustomFieldType, setShowSelectCustomField, onAddCustomField, onRemoveCustomField, onModifyCustomField, toggleFormIsOpen, setEditMode } from '../reduxStore/records/recordSlice';
import { updateRecord, addNewRecord } from '../reduxStore/api/apiSlice';
import { onExitFormWithouSaving } from '../reduxStore/modal/modalSlice';


const RecordForm = () => {
  const recordFormData = useSelector((state) => state.recordForm.recordFormData);
  const isEdited = useSelector((state) => state.recordForm.isEdited);
  const editMode = useSelector((state) => state.recordForm.editMode);
  const formIsOpen = useSelector((state) => state.recordForm.formIsOpen);

  const newRecordMode = useSelector((state) => state.recordForm.newRecordMode);
  const selectedCustomFieldType = useSelector((state) => state.recordForm.selectedCustomFieldType);
  const customFieldOptions = useSelector((state) => state.recordForm.customFieldOptions);
  const showSelectCustomField = useSelector((state) => state.recordForm.showSelectCustomField);
  const customFields = useSelector((state) => state.recordForm.customFields);
  

  const dispatch = useDispatch();

  const [showPassword, setShowPassword] = useState(false);
  const [copyButtonToolTipText, setCopyButtonToolTipText] = useState('copy');

  const saveRecord = () => {
    console.log('record form', recordFormData);
    if (recordFormData.account === '' && recordFormData.siteName === ''){
      alert('account and site name cannot be empty');
      return;
    }
    // Remove fields with no value at all
    let filteredCustomFields = customFields.filter(field => {
      return !!field.field_name || !!field.field_value;
    })
    // Remove temporary id for new custom field
    filteredCustomFields = filteredCustomFields.map(field => {
      let field_info = {
        field_name : field.field_name,
        field_value: field.field_value,
        field_type: field.field_type,
      }
      if(typeof field.id == 'string' && field.id.includes('temporary')){
        return {
          ...field_info,
        };
      }
      else{
        return {
          ...field_info,
          id: field.id
        }
      }
    })
   
    if(newRecordMode){
      dispatch(addNewRecord({recordFormData, filteredCustomFields}));
    }
    else{
      dispatch(updateRecord({recordFormData, filteredCustomFields}));
    }
  }

  const handleChangeRecord = (e, key) => {
    dispatch(updateRecordForm({key, value: e.target.value}));
    if(!isEdited){
      dispatch(setIsEdited(true));
    }
  }

  const closeEditForm = () => {
    if(newRecordMode){
      if(isEdited){
        dispatch(onExitFormWithouSaving());
      }
      else{
        dispatch(toggleFormIsOpen());
      }
    }
    else{
      dispatch(toggleFormIsOpen());
    }
  }

  const copyToClipboard = () => {
    navigator.clipboard.writeText(recordFormData?.password || '')
    .then(() => {
      setCopyButtonToolTipText('copied');
    }, (rej) => {
      console.log(rej);
    }).catch(err => {
      console.log(err);
    })
  }

  const onOpenCopyButtonTooltip = () => {
    setCopyButtonToolTipText('copy');
  }

  const handleUpdateCustomField = (e, type, id) => {
    dispatch(onModifyCustomField({ [type]: e.target.value, id}));
  }

  const capitalizeString = (input) => {
    if(typeof input === 'string'){
      return input.slice(0,1).toUpperCase() + input.slice(1);
    }
  }

  const handleExitEditMode = () => {
    if(isEdited){
      dispatch(onExitFormWithouSaving());
    }
    else{
      dispatch(exitEditMode());
      setShowPassword(false);
    }
  }

  useEffect(() => {
    if(newRecordMode){
      const formDataIsEmpty = (recordFormData.siteName === '') && (recordFormData.account === '') && (recordFormData.password === '');
      formDataIsEmpty && dispatch(setIsEdited(false));
    }  
  }, [recordFormData])

  return (
    <>
    <Box className='recordForm'>
      <Box className={(formIsOpen ? 'active ' : '') + 'recordForm__overlay'} >
      </Box>
      <Box className={(formIsOpen ? 'active ' : '') + 'recordForm__edit-form'}>
        <Stack className='recordForm__edit-form__wrapper'>
          <Box sx={{display: 'flex', justifyContent: 'space-between'}}>
            <Button type='button' onClick={() => closeEditForm()} className='recordForm__closeButton' sx={{visibility: editMode || (newRecordMode && isEdited) ? 'hidden' : 'visible'}}>
              <ChevronRightIcon
                color='info'
                fontSize='medium'
                />
            </Button>
            <Box>
              <Button type='button' onClick={() => saveRecord()}  sx={{visibility: editMode || newRecordMode ? 'visible' : 'hidden'}} disabled={!isEdited}>
                  save
              </Button>
              <Button type='button' onClick={() => dispatch(setEditMode(true))} sx={{display: !editMode && !newRecordMode ? 'initial' : 'none'}}>
                edit
              </Button>
              <Button type='button' onClick={() => handleExitEditMode()} sx={{display: editMode || (newRecordMode && isEdited) ? 'initial' : 'none'}}>
                cancel
              </Button>
            </Box>
          </Box>
          <Typography variant='h4' className='recordForm__title'>edit record</Typography>
          <FormControl className='recordForm__fields'>
            <Stack spacing={1} className='recordForm__stack'>
              <TextField
                required
                id='form-site-name'
                label='Site Name'
                variant='standard'
                onChange={(e) => handleChangeRecord(e, 'siteName')}
                value={recordFormData?.siteName || ''}
                disabled={!editMode && !newRecordMode}
              />
              <TextField
                required
                id='form-account'
                label='Account'
                variant='standard'
                onChange={(e) => handleChangeRecord(e, 'account')}
                value={recordFormData?.account || ''}
                disabled={!editMode && !newRecordMode}
              />
              <Box className='recordForm__password'>
                <TextField
                  className='recordForm__password-field'
                  id='form-password'
                  label='Password'
                  variant='standard'
                  type={showPassword ? 'text' : 'password'}
                  onChange={(e) => handleChangeRecord(e, 'password')}
                  value={recordFormData?.password || ''}
                  disabled={!editMode && !newRecordMode}
                />
                <IconButton
                  aria-label='toggle password visibility'
                  onClick={() => setShowPassword(prev => !prev)}
                >
                  {showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
                <Tooltip 
                  title={copyButtonToolTipText} 
                  placement='top'
                  onOpen={() => onOpenCopyButtonTooltip()}
                  >
                  <IconButton
                    className='recordForm__password__copyButton'
                    onClick={() => copyToClipboard()}
                  >
                    <ContentCopyIcon />
                  </IconButton>
                </Tooltip>
              </Box>


              {customFields.map((field) => (
                <Box sx={{ display: 'flex', placeItems: 'center', p: '10px 0' }} key={field.id}>
                    <Box sx={{display: 'flex', flexDirection: 'column', flex: 1}}>
                      {field.field_type !== 'email' && 
                        <TextField
                            variant='standard'
                            type='text'
                            label={`Custom ${capitalizeString(field.field_type)} Name`}
                            value={field.field_name}
                            onChange={(e) => handleUpdateCustomField(e, 'field_name', field.id)}
                            disabled={!editMode && !newRecordMode}
                        />
                      }
                      <TextField
                          variant='standard'
                          type='text'
                          label={`Custom ${capitalizeString(field.field_type)} Value`}
                          value={field.field_value}
                          onChange={(e) => handleUpdateCustomField(e, 'field_value', field.id)}
                          disabled={!editMode && !newRecordMode}
                      />
                  </Box>
                  <IconButton onClick={() => dispatch(onRemoveCustomField(field.id))} sx={{visibility: editMode || newRecordMode ? 'visible' : 'hidden'}}>
                      <DeleteIcon color='error' />
                  </IconButton>
                </Box>
              ))}


              <Button variant='text' size='small' onClick={() => dispatch(setShowSelectCustomField(true))} sx={{visibility: editMode || newRecordMode ? 'visible' : 'hidden'}}>
                <Typography>new custom field</Typography>
              </Button>

              <FormControl className={showSelectCustomField ? 'show ' : '' + 'custom-field-select'} sx={{visibility: editMode || newRecordMode ? 'visible' : 'hidden'}}>
                <InputLabel id='custom-field-select-label'>Select a Custom Field</InputLabel>
                <Select
                    sx={{textTransform: 'capitalize'}}
                    labelId='custom-field-select-label'
                    id='custom-field-select'
                    value={selectedCustomFieldType}
                    onChange={(e) => dispatch(onSelectCustomFieldType(e.target.value))}
                    input={<OutlinedInput label='Select a Custom Field' />}
                >
                  {customFieldOptions.map(option => (
                     <MenuItem key={option} value={option} className='custom-field-select-item-text'>
                      <em>{option}</em>
                     </MenuItem>
                  ))}
                </Select>
                <Box className='custom-field-select-actions' sx={{display: 'flex', justifyContent:'end', columnGap: 1, mt: 1}}>
                  <Button type='button' variant='contained' color='primary' size='small' onClick={() => dispatch(onAddCustomField({type: selectedCustomFieldType}))}>add</Button>
                  <Button type='button' variant='outlined' colro='info' size='small' onClick={() => dispatch(setShowSelectCustomField(false))}>cancel</Button>
                </Box>
                </FormControl>
            </Stack>


          </FormControl>
          
        </Stack>

      </Box>
    </Box>
    </>
  )
}


export default RecordForm;