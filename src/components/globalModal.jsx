import React from 'react';
import '../css/globalModal.css';
import { Typography, Box, Modal, Button } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { onCloseModal } from '../reduxStore/modal/modalSlice';
import { deleteRecord } from '../reduxStore/api/apiSlice';
import { exitEditMode, resetRecordForm } from '../reduxStore/records/recordSlice';

const GlobalModal = () => {
  const showModal = useSelector((state) => state.modal.showModal);
  const modalMessage = useSelector((state) => state.modal.modalMessage);
  const modalAction = useSelector((state) => state.modal.modalAction);
  const actionContext = useSelector((state) => state.modal.actionContext);

  const dispatch = useDispatch();

  // move this to slice?
  const modalFunctionsMap = {
    'confirmDelete': (id) => {
      dispatch(deleteRecord(id));
    },
    'cancel': () => {
      dispatch(onCloseModal());
    },
    'ok' : () => {

    },
    'confirmExit': () => {
      dispatch(onCloseModal());
      dispatch(exitEditMode());
      dispatch(resetRecordForm());
    }
  }
  return (
    <Modal
      open={showModal}
      onClose={() => dispatch(onCloseModal())}
    >
      <Box className='globalModal'>
        <Typography>
          {modalMessage}
        </Typography>
        <Box className='globalModal__buttons'>
          {modalAction.map(item => (
            <Button key={`modalAction-${item.name}`} size='small' color={item.color} variant={item.variant} onClick={() => modalFunctionsMap[item.name](actionContext)}>
              {item.text}
            </Button>
          ))}
        </Box>

      </Box>

    </Modal>
  )
}

export default GlobalModal; 