import React, { useState, useEffect } from 'react';
import { useContext } from 'react';
import { AuthContext } from '../context/authContext';
import { Link } from 'react-router-dom';
import '../css/navbar.css';
import {
    Button,
    Typography,
    Box,
} from '@mui/material';
const Navbar = () => {
    // const navigate = useNavigate();
    const { logoutUser, isAdmin } = useContext(AuthContext);
    const [indexPath, setIndexPath] = useState('');
    useEffect(() => {
        //isAdmin is true when logged in via admin portal
        setIndexPath(() => isAdmin ? '/admin/residents' : '/home');

    }, [isAdmin])

    return (
        <Box className='nav'>
            <Box className='nav__wrapper'>
                <Typography component={'h1'} className='nav__main-title'><Link to={indexPath}>My Password Manager</Link></Typography >
                <Button variant={'contained'} className='nav__button' sx={{color: '#000'}}  onClick={() => logoutUser()}>log out</Button>
            </Box>

        </Box>
    )
}

export default Navbar;