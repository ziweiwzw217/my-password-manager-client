import React from "react";
import { useContext, useState } from "react";
import { AuthContext } from "../context/authContext";
import '../css/register.css';
import { Link } from "react-router-dom";
import { FormControl, InputLabel, OutlinedInput, Button } from '@mui/material';

const fields = [
    'username',
    'email',
    'password',
    'password2',
    'firstname',
    'lastname',
]

const generateRegisterFormObject = () => {
    return fields.reduce((prev, curr) => {
        prev[curr] = '';
        return prev;
    }, {});
}

const INIT_FORM = generateRegisterFormObject(fields);

const Register = () => {
    const { registerUser } = useContext(AuthContext);
    const [registerForm, setRegisterForm] = useState(INIT_FORM);

    const handleSubmit = (e) => {
        e.preventDefault();
        let { username, email, password, password2 } = registerForm;
        registerUser(username, email, password, password2);
    }

    const handleChange = (e, field) => {
        setRegisterForm(prev => {
            let next = {
                ...prev,
                [field]: e.target.value
            }
            return next;
        })
    }

    const setFieldType = (field) => {
        if(field === 'email') return 'email';
        return field.includes('password') ? 'password' : 'text';
    }
    return (
        <div className="register">
            <div className="register__wrapper">
                <h1 className="register__title"><span>Register</span> My Resident</h1>
                <form className="register__form" onSubmit={handleSubmit}>
                    {fields.map((field) => (
                        <FormControl key={field} fullWidth sx={{ mb: 1 }}>
                            <InputLabel htmlFor={`outlined-adornment-${field}`}>{field}</InputLabel>
                            <OutlinedInput
                                sx={{ backgroundColor: '#fff' }}
                                id={`outlined-adornment-${field}`}
                                value={registerForm[field]}
                                onChange={(e) => handleChange(e, field)}
                                label={field}
                                required={!field.includes('first') && !field.includes('last')}
                                type={setFieldType(field)}
                            />
                        </FormControl>
                    )
                    )}
                    <Button variant='contained' sx={{ color: '#000', backgroundColor: '#61DAFB' }} type='submit'>register</Button>

                </form>


                <span>or</span>
                <Link to={'/login'}>log in</Link>
            </div>

        </div>
    )
}

export default Register;
