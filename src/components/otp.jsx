import React, { useState, useEffect } from 'react';
import { useContext } from 'react';
import { AuthContext } from '../context/authContext';
import { Navigate, useNavigate } from 'react-router-dom';
import useAxios from '../utils/useAxios';
import * as URL from '../constants/urls';
import '../css/otp.css';
import { FormControl, InputLabel, OutlinedInput, Button } from '@mui/material';

const Otp = () => {
    const navigate = useNavigate();
    const axios_private = useAxios();
    const { user, enableModal } = useContext(AuthContext);
    const [otp, setOtp] = useState('');
    const [resendTimer, setResendTimer] = useState(0);
    const [intervalId, setIntervalId] = useState(null);
    const getOtp = (email) => {
        let msg = [];
        axios_private({
            url: URL.GET_OTP,
            method: 'POST',
            data: {
                email
            }
        })
            .catch(err => {
                msg.push(err.response.data.message);
                enableModal(msg, 'error');
            })
    }

    const verifyOtp = (email) => {
        let msg = [];
        axios_private({
            url: URL.VERIFY_OTP,
            method: 'POST',
            data: {
                otp,
                email
            }

        })
            .then(res => {
                if (res.status === 200) {
                    // setUser_verified(true);
                    navigate('/home');
                }
            })
            .catch(err => {
                msg.push(err.response.data.message);

                enableModal(msg, 'error');
            })
    }

    const handleClick = () => {
        getOtp(user.email);
        setResendTimer(5);
        const interval = setInterval(() => {
            setResendTimer(prev => prev - 1);
        }, 1000);
        setIntervalId(interval);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        verifyOtp(user.email);
    }

    useEffect(() => {
        if (resendTimer === 0) {
            clearInterval(intervalId);
        }
    }, [resendTimer])

    return (
        <>
            {!user ? <Navigate to={'/login'} /> :
                (<div className='otp'>
                    <div className='otp__wrapper'>
                        <h1 className='otp__title'><span>verify</span> My Resident</h1>
                        <form className='otp__form' onSubmit={handleSubmit}>
                            <p className='otp__form--desc'>We need to verify that this is you. Click the <span>send</span> button below and we will send an OTP to the email of this account. Enter the OTP below to proceed the verification</p>
                            <FormControl fullWidth sx={{ mb: 1 }}>
                                <InputLabel htmlFor={`outlined-adornment-otp`}>{'Enter your OTP here'}</InputLabel>
                                <OutlinedInput
                                    sx={{ backgroundColor: '#fff' }}
                                    id={`outlined-adornment-otp`}
                                    value={otp}
                                    onChange={(e) => setOtp(e.target.value)}
                                    label={'Enter your OTP here'}
                                    required
                                    type={'text'}
                                />
                            </FormControl>
                            <Button variant='contained' sx={{ color: '#000', backgroundColor: '#61DAFB' }} type='submit' disabled={otp === ''}>verify</Button>

                        </form>
                        <Button variant='contained' sx={{ color: '#000', backgroundColor: '#61DAFB' }} type='button' onClick={handleClick} disabled={resendTimer !== 0}>send <span>{!!resendTimer && `(${resendTimer}s)`}</span></Button>

                    </div>

                </div>)
            }
        </>
    )
}

export default Otp;