import React, { useState } from 'react';
import '../css/signIn.css';
import { useContext } from 'react';
import { AuthContext } from '../context/authContext';
import { Link } from 'react-router-dom';
import { FormControl, InputLabel, OutlinedInput, InputAdornment, IconButton, Button } from '@mui/material';
import { VisibilityOff, Visibility } from '@mui/icons-material';

const SignIn = () => {
  const { loginUser, loginAdminUser, isAdminLogin } = useContext(AuthContext);
  const [password, setPassword] = useState('');
  const [username, setUsername] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const handleSubmit = (e) => {
    e.preventDefault();
    if (isAdminLogin) {
      loginAdminUser(username, password);
    }
    else {
      loginUser(username, password);
    }
  }

  return (
    <>
      <div className='sign-in'>
        <div className='sign-in__wrapper'>
          {isAdminLogin && <h1 className='text-admin'>admin</h1>}
          <h1 className='sign-in__title'><span>log in</span> My Resident</h1>
          <form className='sign-in__form' onSubmit={handleSubmit}>
            <FormControl fullWidth sx={{ mb: 1 }}>
              <InputLabel htmlFor="outlined-adornment-username">{'username'}</InputLabel>
              <OutlinedInput
                sx={{ backgroundColor: '#fff' }}
                id="outlined-adornment-username"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                label={'username'}
              />
            </FormControl>
            <FormControl fullWidth sx={{ mb: 1 }}>
              <InputLabel htmlFor="outlined-adornment-password">{'password'}</InputLabel>
              <OutlinedInput
                sx={{ backgroundColor: '#fff' }}
                id="outlined-adornment-password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                label={'password'}
                type={showPassword ? 'text' : 'password'}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={() => setShowPassword(prev => !prev)}
                      onMouseDown={(e) => e.preventDefault()}
                      edge="end"
                    >
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
              />
            </FormControl>
            <Button type='submit' variant='contained' sx={{color: '#000', backgroundColor: '#61DAFB'}}>sign in</Button>

          </form>
          <span>or</span>
          <Link to={'/register'}>register</Link>


        </div>
      </div>
    </>
  )
}

export default SignIn;